import React from 'react';
import './style.less';

const AnswerList = (props) => {
  const answerList = props.answerList ? props.answerList : ["", "", "", ""];
  if(answerList.length < 4) {
    for (let i = answerList.length; i < 4; i++) {
      answerList.push("");
    }
  }
  return(
    <div className="answer-list">
      {
        answerList.map((item, index) => {
          return (
            <input key={index} type="text" disabled={true} value={item} className="answer-item"/>
          )
        })
      }
    </div>
  )
};

export default AnswerList;