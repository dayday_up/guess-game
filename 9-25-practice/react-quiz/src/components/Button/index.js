import React from 'react';
import './style.less';
const Button = (props) => {
  const {text, bgColor, onClick} = props;
  return(
    <button className="button" onClick={onClick} style={{backgroundColor: bgColor}}>{text}</button>
  )
};

export default Button;