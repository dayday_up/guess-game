import React from 'react';
import ControlPanel from './ControlPanel';
import './style.less';
import PlayingPanel from './PlayingPanel';
class Game extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      answer: []
    };
    this.handleCreateNewCard = this.handleCreateNewCard.bind(this);
  }
  handleCreateNewCard(answer) {
    this.setState({
      answer: answer
    })
  }
  render() {
    return(
      <div className="game">
        <section className="playing-panel">
          <PlayingPanel answer={this.state.answer}/>
        </section>
        <section className="control-wrapper">
          <ControlPanel answer={this.state.answer} handleCreateNewCard={this.handleCreateNewCard}/>
        </section>
      </div>
    )
  }
}

export default Game;