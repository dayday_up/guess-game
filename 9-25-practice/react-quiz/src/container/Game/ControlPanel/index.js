import React from 'react';
import AnswerList from '../../../components/AnswerList';
import Button from '../../../components/Button';
import './style.less';

class ControlPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answerShow: false
    };
    this.createNewCard = this.createNewCard.bind(this);
    this.generateAnswer = this.generateAnswer.bind(this);
    this.hideAnswer = this.hideAnswer.bind(this);
    this.showAnswer = this.showAnswer.bind(this);
  }

  createNewCard() {
    const answer = this.generateAnswer();
    this.props.handleCreateNewCard(answer);
    this.showAnswer();
    this.hideAnswer();
  }

  generateAnswer() {
    let result = [];
    for (let i = 0; i < 4; i++) {
      let ranNum = Math.ceil(Math.random() * 25);
      result.push(String.fromCharCode(65 + ranNum));
    }
    return result;
  }

  hideAnswer() {
    setTimeout(() => {
      this.setState({answerShow: false})
    }, 3000);
  }

  showAnswer() {
    this.setState({
      answerShow: true
    })
  }

  render() {
    const {answerShow} = this.state;
    const answer = this.props.answer;
    return (
      <div className="control-panel">
        <h2 className="title">New Card</h2>
        <div className="new-card-wrapper">
          <Button onClick={this.createNewCard} bgColor="blue" text="New Card"/>
        </div>
        {
          answerShow ? (
              <div className="answer-list-wrapper">
                <AnswerList answerList={answer}/>
              </div>
            )
            : ""
        }

        <div className="show-result-wrapper">
          <Button bgColor="red" onClick={this.showAnswer} text="Show Result"/>
        </div>
      </div>
    )
  }
}

export default ControlPanel;