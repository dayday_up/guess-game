import React from 'react';
import AnswerList from '../../../components/AnswerList';
import Button from '../../../components/Button';

class PlayingPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      guessAnswer: "",
      result: "FAILED"
    };
    this.calculateResult = this.calculateResult.bind(this);
    this.handleGuess = this.handleGuess.bind(this);
  }

  calculateResult() {
    const { answer } = this.props;
    const { guessAnswer } = this.state;
    this.setState({
      result: answer.join("") === guessAnswer && answer.join("") !== "" ? "SUCCESS" : "FAILED"
    })
  }
  handleGuess(e) {
    const guessAnswer = e.target.value;
    if(guessAnswer.length <= 4) {
      this.setState({
        guessAnswer: guessAnswer
      })
    }
  }

  render() {
    const { result } = this.state;
    const answerList = this.state.guessAnswer.split("");
    return (
      <div className="playing-panel">
        <div className="your-result">
          <h2 className="title">Your Result</h2>
          <AnswerList answerList={answerList}/>
          <div className="result">{ result }</div>
        </div>
        <div className="guess-card">
          <h2 className="title">Your Result</h2>
          <div className="input-wrapper">
            <input type="text" pattern="[A-Z]" className="guess-input" onChange={this.handleGuess}/>
          </div>
          <Button bgColor="green" text="Guess" onClick={this.calculateResult}/>
        </div>
      </div>
    )
  }
}

export default PlayingPanel;


