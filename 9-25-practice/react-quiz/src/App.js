import React from 'react';
import Game from './container/Game';

class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      answer: []
    }
  }
  render() {
    return (
      <Game />
    )
  }
}

export default App;