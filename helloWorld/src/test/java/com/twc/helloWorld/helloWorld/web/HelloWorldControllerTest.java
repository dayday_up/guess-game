package com.twc.helloWorld.helloWorld.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class HelloWorldControllerTest {
    @Autowired
    private MockMvc mockMvc;
    MvcResult mvcResult;
    @BeforeEach
    void setUp() throws Exception {
        mvcResult = mockMvc.perform(get("/")).andReturn();
    }

    @Test
    void it_should_return_200_as_status() throws Exception {
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    void it_should_return_value_of_hello_world() throws UnsupportedEncodingException {
        assertThat(mvcResult.getResponse().getContentAsString()).isEqualTo("hello world");
    }
}
