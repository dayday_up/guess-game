package com.twc.helloWorld.helloWorld.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    @GetMapping
    public static String main(String[] args) {
        return "hello world";
    }
}
